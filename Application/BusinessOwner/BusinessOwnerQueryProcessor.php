<?php

declare(strict_types=1);

namespace App\Places\Application\BusinessOwner;

use App\Places\Application\PlaceQueryProcessor;

final class BusinessOwnerQueryProcessor
{
    private const MONTH = 30;
    private const YEAR = 360;

    private PlaceQueryProcessor $placeQueryProcessor;

    private string $premiumOwnerPackageMonth;

    private string $premiumOwnerPackageYear;

    private string $regularOwnerPackageYear;

    public function __construct(
        PlaceQueryProcessor $placeQueryProcessor,
        string $premiumOwnerPackageMonth,
        string $premiumOwnerPackageYear,
        string $regularOwnerPackageYear
    ) {
        $this->placeQueryProcessor = $placeQueryProcessor;
        $this->premiumOwnerPackageMonth = $premiumOwnerPackageMonth;
        $this->premiumOwnerPackageYear = $premiumOwnerPackageYear;
        $this->regularOwnerPackageYear = $regularOwnerPackageYear;
    }

    public function getPremiumOwnerPackageMonth(): string
    {
        return $this->premiumOwnerPackageMonth;
    }

    public function getPremiumOwnerPackageYear(): string
    {
        return $this->premiumOwnerPackageYear;
    }

    public function getRegularOwnerPackageYear(): string
    {
        return $this->regularOwnerPackageYear;
    }

    public function getRecurringPeriodInMonth(): int
    {
        return self::MONTH;
    }

    public function getRecurringPeriodInYear(): int
    {
        return self::YEAR;
    }

    /**
     * @throws PlaceHasPlaceowner
     * @throws \App\Places\Application\PlaceNotFound
     */
    public function calculateTotalCoast(int $placeId, string $ownerPackage, int $recurringPeriod): string
    {
        if ($this->hasOwner($placeId)) {
            throw new PlaceHasPlaceowner();
        }

        return $this->checkAmount($ownerPackage, $recurringPeriod);
    }

    private function hasOwner(int $placeId): bool
    {
        $place = $this->placeQueryProcessor->getById($placeId);
        return null !== $place->getOwnerId();
    }

    private function checkAmount(string $ownerPackage, int $recurringPeriod): string
    {
        if ('premiumOwnerPackageMonth' === $ownerPackage && $recurringPeriod === self::MONTH) {
            return $this->premiumOwnerPackageMonth;
        }

        if ('premiumOwnerPackageYear' === $ownerPackage && $recurringPeriod === self::YEAR) {
            return $this->premiumOwnerPackageYear;
        }

        if ('regularOwnerPackageYear' === $ownerPackage && $recurringPeriod === self::YEAR) {
            return $this->regularOwnerPackageYear;
        }

        return '';
    }
}
