<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\Editable;

final class ValueOfEditable
{
    public static function requestedToBeRemove(): int
    {
        return Editable::requestedToBeRemove()->getRawValue();
    }

    public static function newlyAdded(): int
    {
        return Editable::newlyAdded()->getRawValue();
    }

    public static function enabled(): int
    {
        return Editable::enabled()->getRawValue();
    }
}
