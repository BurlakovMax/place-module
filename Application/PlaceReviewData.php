<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceReview;
use DateTimeImmutable;
use Swagger\Annotations as SWG;

final class PlaceReviewData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $placeId;

    /**
     * @SWG\Property()
     */
    private int $accountId;

    /**
     * @SWG\Property()
     */
    private int $providerId;

    /**
     * @SWG\Property()
     */
    private DateTimeImmutable $reviewDate;

    /**
     * @SWG\Property()
     */
    private string $comment;

    /**
     * @SWG\Property()
     */
    private $star;

    /**
     * @SWG\Property()
     */
    private ?string $ip;

    public function __construct(PlaceReview $placeReview)
    {
        $this->id = $placeReview->getId();
        $this->placeId = $placeReview->getPlaceId();
        $this->accountId = $placeReview->getAccountId();
        $this->providerId = $placeReview->getProviderId();
        $this->reviewDate = $placeReview->getReviewDate();
        $this->comment = $placeReview->getComment();
        $this->star = $placeReview->getStar()->getRawValue();
        $this->ip = $placeReview->getIp();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getProviderId(): int
    {
        return $this->providerId;
    }

    public function getReviewDate(): DateTimeImmutable
    {
        return $this->reviewDate;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getStar(): int
    {
        return $this->star;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }
}