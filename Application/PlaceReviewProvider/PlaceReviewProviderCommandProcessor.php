<?php

declare(strict_types=1);

namespace App\Places\Application\PlaceReviewProvider;

use App\Places\Domain\PlaceReviewProvider\PlaceReviewProvider;
use App\Places\Domain\PlaceReviewProvider\PlaceReviewProviderWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class PlaceReviewProviderCommandProcessor
{
    private TransactionManager $transactionManager;

    private PlaceReviewProviderWriteStorage $providerWriteStorage;

    public function __construct(
        TransactionManager $transactionManager,
        PlaceReviewProviderWriteStorage $providerWriteStorage
    ) {
        $this->transactionManager = $transactionManager;
        $this->providerWriteStorage = $providerWriteStorage;
    }

    /**
     * @throws \Throwable
     */
    public function add(CreatePlaceReviewProviderCommand $providerCommand): int
    {
        $provider = new PlaceReviewProvider(
            $providerCommand->getName(),
            $providerCommand->getPlaceId(),
            $providerCommand->getImageUploadCount()
        );

        $this->transactionManager->transactional(function () use ($provider) {
            $this->providerWriteStorage->create($provider);
        });

        return $provider->getId();
    }
}