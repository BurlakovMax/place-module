<?php

declare(strict_types=1);

namespace App\Places\Application;

use LazyLemurs\Commander\Property;

final class PlaceSocialNetworksCommand
{
    /**
     * @Property(type="?string")
     */
    private ?string $facebook;

    /**
     * @Property(type="?string")
     */
    private ?string $twitter;

    /**
     * @Property(type="?string")
     */
    private ?string $instagram;

    /**
     * @Property(type="?string")
     */
    private ?string $whatsapp;

    /**
     * @Property(type="?string")
     */
    private ?string $line;

    /**
     * @Property(type="?string")
     */
    private ?string $wechat;

    /**
     * @Property(type="?string")
     */
    private ?string $telegram;

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function getLine(): ?string
    {
        return $this->line;
    }

    public function getWechat(): ?string
    {
        return $this->wechat;
    }

    public function getTelegram(): ?string
    {
        return $this->telegram;
    }
}
