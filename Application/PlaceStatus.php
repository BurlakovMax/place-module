<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Core\DomainSupport\Enumerable;

final class PlaceStatus extends Enumerable
{
    public static function active(): self
    {
        return self::createEnum('active');
    }

    public static function deleted(): self
    {
        return self::createEnum('deleted');
    }
}