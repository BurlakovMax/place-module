<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\AttributeOptions;
use App\Places\Domain\PlaceTypeAttribute;
use Swagger\Annotations as SWG;

final class PlaceTypeAttributeData
{
    /**
     * @SWG\Property()
     */
    private int $placeTypeId;

    /**
     * @SWG\Property()
     */
    private int $attributeId;

    /**
     * @var AttributeOptions[]
     *
     * @SWG\Property(property="attributes", type="array", @SWG\Items(type="string"))
     */
    private array $options;

    public function __construct(PlaceTypeAttribute $placeTypeAttribute)
    {
        $this->placeTypeId = $placeTypeAttribute->getPlaceTypeId();
        $this->attributeId = $placeTypeAttribute->getAttributeId();
        $this->options = $placeTypeAttribute->getOptions();
    }

    public function getPlaceTypeId(): int
    {
        return $this->placeTypeId;
    }

    public function getAttributeId(): int
    {
        return $this->attributeId;
    }

    /**
     * @return AttributeOptions[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}