<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Core\Application\Search\SearchQuery;
use App\Places\Domain\Place;
use App\Places\Domain\PlaceReadStorage;

final class PlaceQueryProcessor
{
    private PlaceReadStorage $placeReadStorage;

    public function __construct(PlaceReadStorage $placeReadStorage)
    {
        $this->placeReadStorage = $placeReadStorage;
    }

    /**
     * @throws PlaceNotFound
     */
    public function getById(int $id): PlaceData
    {
        return $this->mapToData(
            $this->get($id)
        );
    }

    /**
     * @return PlaceData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $list = $this->placeReadStorage->getBySearchQuery($query);

        return $this->mapListToData($list);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->placeReadStorage->countBySearchQuery($query);
    }

    /**
     * @param <string>[] $groupingFields
     * @return <string, string>[][]
     */
    public function getDuplicates(SearchQuery $query, array $groupingFields): array
    {
        return $this->placeReadStorage->getDuplicates($query, $groupingFields);
    }

    /**
     * @param <string>[] $groupingFields
     * @return int
     */
    public function getDuplicatesCount(SearchQuery $query, array $groupingFields): int
    {
        return $this->placeReadStorage->getDuplicatesCount($query, $groupingFields);
    }

    /**
     * @param Place[] $list
     * @return PlaceData[]
     */
    private function mapListToData(array $list): array
    {
        return array_map(
            function (Place $place): PlaceData {
                return $this->mapToData($place);
            },
            $list
        );
    }

    /**
     * @return int[]
     */
    public function getTypeIdsByLocationId(int $locationId): array
    {
        return
            array_map(
                function (array $type): int {
                    return $type['typeId'];
                },
                $this->placeReadStorage->getTypeIdsByLocationId($locationId)
            )
        ;
    }

    /**
     * @param int[] $locationIds
     */
    public function count(int $typeId, array $locationIds): int
    {
        return $this->placeReadStorage->countByCriteria($typeId, $locationIds);
    }

    /**
     * @throws PlaceNotFound
     */
    private function get(int $id): Place
    {
        $place = $this->placeReadStorage->get($id);

        if (!$place) {
            throw new PlaceNotFound();
        }

        return $place;
    }

    private function mapToData(Place $place): PlaceData
    {
        return new PlaceData($place);
    }
}