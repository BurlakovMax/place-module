<?php

declare(strict_types=1);

namespace App\Places\Application;

use LazyLemurs\Commander\Property;

final class PlaceAddressCommand
{
    /**
     * @Property(type="?string")
     */
    private ?string $address1;

    /**
     * @Property(type="?string")
     */
    private ?string $address2;

    /**
     * @Property(type="?string")
     */
    private ?string $zipCode;

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }
}