<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Adapter;

use App\EmailGate\Domain\EmailData;
use App\EmailGate\Domain\MessageSender;
use App\Places\Domain\EmailProvider;

final class EmailGateAdapter implements EmailProvider
{
    private MessageSender $messageSender;

    public function __construct(MessageSender $messageSender)
    {
        $this->messageSender = $messageSender;
    }

    public function sendSync(EmailData $emailData): void
    {
        $this->messageSender->sendSync($emailData);
    }
}