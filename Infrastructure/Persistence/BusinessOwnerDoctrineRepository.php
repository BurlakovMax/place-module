<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Places\Domain\BusinessOwner\BusinessOwner;
use App\Places\Domain\BusinessOwner\BusinessOwnerWriteStorage;
use Doctrine\ORM\EntityRepository;

final class BusinessOwnerDoctrineRepository extends EntityRepository implements BusinessOwnerWriteStorage
{
    public function add(BusinessOwner $businessOwner): void
    {
        $this->getEntityManager()->persist($businessOwner);
    }
}