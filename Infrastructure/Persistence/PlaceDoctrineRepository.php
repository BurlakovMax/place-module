<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Grouping;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Places\Domain\Editable;
use App\Places\Domain\Place;
use App\Places\Domain\PlaceReadStorage;
use App\Places\Domain\PlaceWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class PlaceDoctrineRepository extends EntityRepository implements PlaceReadStorage, PlaceWriteStorage
{
    public function getAndLock(int $id): ?Place
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    public function get(int $id): ?Place
    {
        return $this->find($id);
    }

    /**
     * @return Place[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.name',
                            't.address1',
                            't.address2',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.name',
                            't.address.address1',
                            't.address.address2',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @param int[] $locationIds
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countByCriteria(int $typeId, array $locationIds): int
    {
        $count =
            $this->createQueryBuilder('t')
            ->addCriteria($this->createCriteriaBy($typeId, $locationIds))
            ->select('COUNT(t.id) as count')
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @return int[]
     * @throws QueryException
     */
    public function getTypeIdsByLocationId(int $locationId): array
    {
        return
            $this->createQueryBuilder('t')
                ->select('t.typeId')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('t.locationId', $locationId))
                        ->andWhere(Criteria::expr()->eq('t.editable', Editable::enabled()))
                )
                ->groupBy('t.typeId')
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @param <string>[] $groupingFields
     * @return <string, string>[][]
     * @throws QueryException
     */
    public function getDuplicates(SearchQuery $query, array $groupingFields): array
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->select('t.name', 't.locationId', 't.address.address1 as address', 't.contact.phone1 as phone')
            ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
            ->addCriteria(
                Limitation::limitation($query->getLimitation())
            )
            ->having('COUNT(t.id) > 1')
        ;

        Grouping::add($queryBuilder, $groupingFields);
        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @param <string>[] $groupingFields
     * @return int
     * @throws QueryException
     */
    public function getDuplicatesCount(SearchQuery $query, array $groupingFields): int
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->select('t.id')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->having('COUNT(t.id) > 1')
        ;

        Grouping::add($queryBuilder, $groupingFields);

        $result = $queryBuilder
            ->getQuery()
            ->getResult()
        ;

        return count($result);
    }

    /**
     * @throws ORMException
     */
    public function create(Place $place): void
    {
        $this->getEntityManager()->persist($place);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws ORMException
     */
    public function forceDelete(Place $place): void
    {
        $this->getEntityManager()->remove($place);
    }

    /**
     * @param int[] $locationIds
     * @return Criteria
     */
    private function createCriteriaBy(int $typeId, array $locationIds): Criteria
    {
        return
            Criteria::create()
                ->andWhere(Criteria::expr()->eq('t.typeId', $typeId))
                ->andWhere(Criteria::expr()->in('t.locationId', $locationIds))
                ->andWhere(Criteria::expr()->eq('t.editable', Editable::enabled()))
                ->andWhere(Criteria::expr()->isNull('t.datesOfChanges.deletedAt'))
            ;
    }
}
