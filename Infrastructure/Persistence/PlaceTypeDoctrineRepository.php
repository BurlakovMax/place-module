<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Places\Domain\PlaceType;
use App\Places\Domain\PlaceTypeReadStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\QueryException;

final class PlaceTypeDoctrineRepository extends EntityRepository implements PlaceTypeReadStorage
{
    public function get(int $id): ?PlaceType
    {
        return $this->find($id);
    }

    /**
     * @return PlaceType[]
     */
    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getOneByUrl(string $url): ?PlaceType
    {
        return $this->findOneBy(['url' => $url]);
    }

    /**
     * @param int[] $ids
     * @return PlaceType[]
     * @throws QueryException
     */
    public function getByIds(array $ids): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->in('id', $ids))
                )
                ->getQuery()
                ->getResult()
            ;
    }
}