<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Places\Domain\PlaceReview;
use App\Places\Domain\PlaceReviewReadStorage;
use App\Places\Domain\PlaceReviewWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class PlaceReviewDoctrineRepository extends EntityRepository implements PlaceReviewReadStorage, PlaceReviewWriteStorage
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(PlaceReview $placeReview): void
    {
        $this->getEntityManager()->persist($placeReview);
        $this->getEntityManager()->flush();
    }

    public function getAndLock(int $id): ?PlaceReview
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    /**
     * @throws ORMException
     */
    public function delete(PlaceReview $placeReview): void
    {
        $this->getEntityManager()->remove($placeReview);
    }

    /**
     * @return PlaceReview[]
     *
     * @throws QueryException
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this->createQueryBuilder('t')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws QueryException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    public function get(int $id): ?PlaceReview
    {
        return $this->find($id);
    }

    /**
     * @return PlaceReview[]
     *
     * @throws QueryException
     */
    public function getByPlaceId(int $placeId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('placeId', $placeId))
                )
                ->orderBy('t.reviewDate', Criteria::ASC)
                ->getQuery()
                ->getResult()
            ;
    }

    public function countAll(): int
    {
        return $this->count([]);
    }
}
