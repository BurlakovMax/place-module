<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence\Mapping;

use App\Places\Domain\Editable;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class EditableType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Editable::class;
    }

    public function getName()
    {
        return 'editable';
    }
}