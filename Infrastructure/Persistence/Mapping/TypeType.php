<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence\Mapping;

use App\Places\Domain\Type;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class TypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Type::class;
    }

    public function getName()
    {
        return 'type_type';
    }
}