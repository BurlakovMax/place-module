<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence\Mapping;

use App\Places\Domain\Sponsorship;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class SponsorshipType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Sponsorship::class;
    }

    public function getName()
    {
        return 'sponsorship';
    }
}