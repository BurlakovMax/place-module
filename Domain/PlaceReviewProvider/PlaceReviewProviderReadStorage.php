<?php

declare(strict_types=1);

namespace App\Places\Domain\PlaceReviewProvider;

use Doctrine\ORM\Query\QueryException;

interface PlaceReviewProviderReadStorage
{
    /**
     * @return PlaceReviewProvider[]
     * @throws QueryException
     */
    public function getByPlaceId(int $placeId): array;
}