<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\DomainSupport\Enumerable;

final class Editable extends Enumerable
{
    public static function requestedToBeRemove(): self
    {
        return self::createEnum(-2);
    }

    public static function newlyAdded(): self
    {
        return self::createEnum(0);
    }

    public static function enabled(): self
    {
        return self::createEnum(1);
    }
}