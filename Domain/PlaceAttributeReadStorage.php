<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Query\QueryException;

interface PlaceAttributeReadStorage
{
    /**
     * @return PlaceAttribute[]
     *
     * @throws QueryException
     */
    public function getByPlaceId(int $placeId): array;

    /**
     * @return PlaceAttribute[]
     *
     * @throws QueryException
     */
    public function getByPlaceIdAndLock(int $id): array;

    public function countAll(): int;
}