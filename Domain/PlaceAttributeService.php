<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Places\Application\PlaceAttributeCommand;
use App\Places\Application\UpdatePlaceCommand;
use Doctrine\ORM\Query\QueryException;
use RuntimeException;

final class PlaceAttributeService
{
    private PlaceAttributeWriteStorage $placeAttributeWriteStorage;

    private PlaceAttributeReadStorage $placeAttributeReadStorage;

    private AttributeReadStorage $attributeReadStorage;

    public function __construct(
        PlaceAttributeWriteStorage $placeAttributeWriteStorage,
        AttributeReadStorage $attributeReadStorage,
        PlaceAttributeReadStorage $placeAttributeReadStorage
    ) {
        $this->placeAttributeWriteStorage = $placeAttributeWriteStorage;
        $this->attributeReadStorage = $attributeReadStorage;
        $this->placeAttributeReadStorage = $placeAttributeReadStorage;
    }

    public function create(int $placeId, int $attributeId, ?string $value, ?string $valueText): void
    {
        $attribute = $this->attributeReadStorage->get($attributeId);

        if (null === $attribute) {
            throw new RuntimeException('Attribute does not exist');
        }

        $this->placeAttributeWriteStorage->create(new PlaceAttribute($placeId, $attribute, $value, $valueText));
    }

    /**
     * @throws QueryException
     */
    public function update(UpdatePlaceCommand $updatePlaceCommand): void
    {
        $placeAttributes = $this->placeAttributeReadStorage->getByPlaceIdAndLock($updatePlaceCommand->getId());

        foreach ($updatePlaceCommand->getPlaceAttributes() as $attribute) {
            if ($this->updateAttributeIfExist($placeAttributes, $attribute)) {
                continue;
            }

            $this->create(
                $updatePlaceCommand->getId(),
                $attribute->getAttributeId(),
                $attribute->getValue(),
                $attribute->getText()
            );
        }
    }

    /**
     * @param PlaceAttribute[] $placeAttribute
     */
    private function updateAttributeIfExist(array $placeAttribute, PlaceAttributeCommand $placeAttributeCommand): bool
    {
        foreach ($placeAttribute as $attribute) {
            if ($attribute->getAttributeId() === $placeAttributeCommand->getAttributeId()) {
                $attribute->update($placeAttributeCommand);

                return true;
            }
        }

        return false;
    }
}
