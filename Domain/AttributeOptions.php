<?php

declare(strict_types=1);

namespace App\Places\Domain;

final class AttributeOptions
{
    /** @var string */
    private string $title;

    /** @var string */
    private string $type;

    /** @var string[] */
    private array $options;

    /**
     * @param string[] $options
     */
    public function __construct(string $title, string $type, array $options)
    {
        $this->title = $title;
        $this->type = $type;
        $this->options = $options;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}