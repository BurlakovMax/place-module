<?php

declare(strict_types=1);

namespace App\Places\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\PlaceReviewCommentDoctrineRepository")
 * @ORM\Table(name="place_review_comment")
 */
class PlaceReviewComment
{
    /**
     * @var int
     *
     * @ORM\Column(name="comment_id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private int $reviewId;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private int $accountId;

    /**
     * @ORM\Column(name="date", type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="text")
     */
    private string $comment;

    public function __construct(
        int $reviewId,
        int $accountId,
        string $comment
    ) {
        $this->reviewId = $reviewId;
        $this->accountId = $accountId;
        $this->createdAt = new DateTimeImmutable();
        $this->comment = $comment;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReviewId(): int
    {
        return $this->reviewId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getComment(): string
    {
        return $this->comment;
    }
}