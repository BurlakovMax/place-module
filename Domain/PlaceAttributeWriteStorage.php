<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface PlaceAttributeWriteStorage
{
    public function create(PlaceAttribute $placeAttribute): void;
}