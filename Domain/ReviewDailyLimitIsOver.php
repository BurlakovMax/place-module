<?php

declare(strict_types=1);

namespace App\Places\Domain;

use LazyLemurs\Exceptions\Exception;

final class ReviewDailyLimitIsOver extends Exception
{

}