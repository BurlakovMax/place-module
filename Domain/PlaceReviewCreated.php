<?php

declare(strict_types=1);

namespace App\Places\Domain;

use LazyLemurs\DomainEvents\DomainEvent;

final class PlaceReviewCreated extends DomainEvent
{
    private int $id;

    private int $placeId;

    private int $accountId;

    private int $providerId;

    private string $comment;

    public function __construct(int $id, int $placeId, int $accountId, int $providerId, string $comment)
    {
        parent::__construct();
        $this->id = $id;
        $this->placeId = $placeId;
        $this->accountId = $accountId;
        $this->providerId = $providerId;
        $this->comment = $comment;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getProviderId(): int
    {
        return $this->providerId;
    }

    public function getComment(): string
    {
        return $this->comment;
    }
}