<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\Email;

/**
 * @ORM\Embeddable()
 */
final class ContactDetails
{
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $phone1;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $phone2;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $fax;

    /**
     * @ORM\Column(type="email", length=100, nullable=true)
     */
    private ?Email $email;

    public function __construct(?string $phone1, ?string $phone2, ?Email $email, ?string $fax)
    {
        $this->phone1 = $phone1;
        $this->phone2 = $phone2;
        $this->email  = $email;
        $this->fax = $fax;
    }

    public function getPhone1(): ?string
    {
        return $this->phone1;
    }

    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }
}
