<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\AttributeDoctrineRepository")
 * @ORM\Table(name="attribute")
 */
class Attribute
{
    /**
     * @ORM\Column(name="attribute_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $title;

    /**
     * @ORM\Column(name="datatype", type="string", length=10)
     */
    private string $dataType;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $options;

    /**
     * @ORM\Column(type="integer", length=4, nullable=true)
     */
    private ?int $facet;

    public function isTextType(): bool
    {
        return $this->dataType === 'text' || $this->dataType === 'cover';
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDataType(): string
    {
        return $this->dataType;
    }

    public function getOptions(): ?string
    {
        return $this->options;
    }

    public function getFacet(): ?int
    {
        return $this->facet;
    }
}