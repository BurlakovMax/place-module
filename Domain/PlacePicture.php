<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="place_picture")
 */
class PlacePicture
{
    /**
     * @ORM\Column(name="picture", type="integer", length=9)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="place_file_type_id", type="integer", length=6, nullable=true)
     */
    private ?int $typeId;

    /**
     * @ORM\Column(name="id", type="integer", length=8)
     */
    private int $placeId;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private ?string $module;

    /**
     * @ORM\Column(name="filename", type="string", length=60)
     */
    private string $fileName;

    /**
     * @ORM\Column(name="thumb", type="string", length=60, nullable=true)
     */
    private ?string $thumbnail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="integer", length=6, nullable=true)
     */
    private ?int $orden;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $visible;

    /**
     * @ORM\Column(name="loc_id", type="integer", length=11, nullable=true)
     */
    private ?int $locationId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $countPlay;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private int $accountId;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function getLocationId(): ?int
    {
        return $this->locationId;
    }

    public function getCountPlay(): int
    {
        return $this->countPlay;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }
}