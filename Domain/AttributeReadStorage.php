<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface AttributeReadStorage
{
    public function get(int $id): ?Attribute;
}