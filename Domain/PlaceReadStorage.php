<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\Application\Search\SearchQuery;

interface PlaceReadStorage
{
    public function get(int $id): ?Place;

    /**
     * @return Place[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function getAndLock(int $id): ?Place;

    /**
     * @param int[] $locationIds
     */
    public function countByCriteria(int $typeId, array $locationIds): int;

    /**
     * @return int[]
     */
    public function getTypeIdsByLocationId(int $locationId): array;

    /**
     * @param <string> $groupingFields
     * @return <string, string>[][]
     */
    public function getDuplicates(SearchQuery $query, array $groupingFields): array;

    /**
     * @param <string> $groupingFields
     * @return int
     */
    public function getDuplicatesCount(SearchQuery $query, array $groupingFields): int;
}
