<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class SocialNetworks
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $twitter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $instagram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $whatsapp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $line;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $wechat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $telegram;

    public function __construct(
        ?string $facebook,
        ?string $twitter,
        ?string $instagram,
        ?string $whatsapp,
        ?string $line,
        ?string $wechat,
        ?string $telegram
    ) {
        $this->facebook  = $facebook;
        $this->twitter   = $twitter;
        $this->instagram = $instagram;
        $this->whatsapp = $whatsapp;
        $this->line = $line;
        $this->wechat = $wechat;
        $this->telegram = $telegram;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function getLine(): ?string
    {
        return $this->line;
    }

    public function getWechat(): ?string
    {
        return $this->wechat;
    }

    public function getTelegram(): ?string
    {
        return $this->telegram;
    }
}
