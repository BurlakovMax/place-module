<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface PlaceReviewCommentReadStorage
{
    public function get(int $id): ?PlaceReviewComment;

    /**
     * @return PlaceReviewComment[]
     */
    public function getByReviewId(int $reviewId): array;
}