<?php

declare(strict_types=1);

namespace App\Places\Domain\BusinessOwner;

use App\Core\DomainSupport\Enumerable;

final class Status extends Enumerable
{
    public static function active(): self
    {
        return self::createEnum(1);
    }

    public static function disable(): self
    {
        return self::createEnum(0);
    }
}